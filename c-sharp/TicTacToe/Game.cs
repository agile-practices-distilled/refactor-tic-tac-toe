﻿using System;

namespace TicTacToe
{
    public class Game
    {
        private char _lastSymbol = Board.EMPTY_SYMBOL;
        private readonly Board _board = new Board();
        
        public void Play(char symbol, int x, int y)
        {
            VerifyFirstPlayer(symbol);
            VerifyRepeatedPlayer(symbol);
            VerifyTileNotUsed(x,y);

            UpdateGameState(symbol, x, y);
        }

        public char Winner()
        {
            for (int rowIndex = 0; rowIndex < Board.BoardSize; rowIndex++)
            {
                if (ArePositionsTakenWithSameSymbolAtRow(rowIndex))
                {
                    return _board.GetSymbolInTile(rowIndex, 0);
                }
            }

            return Board.EMPTY_SYMBOL;
        }

        private void VerifyFirstPlayer(char symbol)
        {
            if (_lastSymbol == Board.EMPTY_SYMBOL && symbol == 'O')
            {
                throw new Exception("Invalid first player");
            }
        }
        
        private void VerifyRepeatedPlayer(char symbol)
        {
            if (symbol == _lastSymbol)
            {
                throw new Exception("Invalid next player");
            }
        }
        
        private void VerifyTileNotUsed(int x, int y)
        {
            if (_board.IsTileOccupied(x, y))
            {
                throw new Exception("Invalid position");
            }
        }

        private void UpdateGameState(char symbol, int x, int y)
        {
            _lastSymbol = symbol;
            _board.AddTileAt(symbol, x, y);
        }

        private bool ArePositionsTakenWithSameSymbolAtRow(int row)
        {
            return _board.IsTileOccupied(row, 0) &&
                   _board.IsTileOccupied(row, 1) &&
                   _board.IsTileOccupied(row, 2) &&
                   _board.GetSymbolInTile(row, 0) == 
                   _board.GetSymbolInTile(row, 1) &&
                   _board.GetSymbolInTile(row, 2) == 
                   _board.GetSymbolInTile(row, 1);
        }
    }
}