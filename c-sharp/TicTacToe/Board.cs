using System.Collections.Generic;
using System.Linq;

namespace TicTacToe
{
    public class Board
    { 
        public const int BoardSize = 3;
        public const char EMPTY_SYMBOL = ' ';
        
        private List<Tile> _plays = new List<Tile>();
       
        public Board()
        {
            for (int i = 0; i < BoardSize; i++)
            {
                for (int j = 0; j < BoardSize; j++)
                {
                    _plays.Add(new Tile(i, j));
                }  
            }       
        }
        
        public char GetSymbolInTile(int x, int y)
        {
            return GetTile(x,y).Symbol;
        }

        public void AddTileAt(char symbol, int x, int y)
        {
            GetTile(x,y).Update(symbol);
        }

        public bool IsTileOccupied(int x, int y)
        {
            return GetTile(x, y).IsOccupied();
        }

        private Tile GetTile(int x, int y)
        {
            return _plays.Single(tile => tile.X == x && tile.Y == y);
        }
    }
}