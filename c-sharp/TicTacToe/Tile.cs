namespace TicTacToe
{
    public class Tile
    {
        public int X { get; }
        public int Y {get;}
        public char Symbol { get; private set; }

        public Tile(int x, int y, char symbol = Board.EMPTY_SYMBOL)
        {
            X = x;
            Y = y;
            Symbol = symbol;
        }

        public void Update(char symbol)
        {
            Symbol = symbol;
        }

        public bool IsOccupied()
        {
            return Symbol != Board.EMPTY_SYMBOL;
        }
    }
}